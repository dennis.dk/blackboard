import firebase from 'firebase/app'
import 'firebase/firestore'
import 'firebase/auth'

var firebaseConfig = {
    apiKey: "AIzaSyBIgB7uKDR9Dfsy9Hx6tDrcoc7BlmfrXYM",
    authDomain: "blackboard-3fe26.firebaseapp.com",
    databaseURL: "https://blackboard-3fe26.firebaseio.com",
    projectId: "blackboard-3fe26",
    storageBucket: "blackboard-3fe26.appspot.com",
    messagingSenderId: "233164211951",
    appId: "1:233164211951:web:1fbcd42939db12f5247d1a",
    measurementId: "G-L8K2QFVMDH"
  };
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);
  firebase.analytics();

  firebase.firestore().settings({ timestampsInSnapshots: true })

  export default firebase