# Django  & React 

A system for creating online curriculum for students studying at home. This system is designed using the Django REST Framework and React.

## Backend development workflow

```json
virtualenv env
source env/bin/activate
pip install -r requirements.txt
python manage.py runserver
```

## Frontend development workflow

```json
npm i
npm start
```

## For deploying

```json
npm run build
```
